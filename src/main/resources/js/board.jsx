'use strict';

import React from 'react';

class Board extends React.Component {

    render() {
        let rows = this.props.board.map((row, index) =>
            <Row key={index} cells={row} toggleCell={this.props.toggleCell}/>
        );
        
        return (<table className='board'><tbody>{rows}</tbody></table>);
    }
}

class Row extends React.Component {
    
    render() {
        let cells = this.props.cells.map((cell, index) =>
            <Cell key={index} cell={cell} toggleCell={this.props.toggleCell}/>
        );
        return (<tr>{cells}</tr>);
    }
}

class Cell extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.cellClick = this.click.bind(this);
    }
    
    click() {
        this.props.toggleCell(this.props.cell);
    }
    
    render() {
        let className = this.props.cell.alive ? 'alive' : 'dead';
        
        return (<td className={className} onClick={this.cellClick}></td>);
    }
}

module.exports = Board;
