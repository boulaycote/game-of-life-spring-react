'use strict';

const Game = require('./game').Game;
const Cell = require('./game').Cell;

test('Game rows and cols counts should match height and width', () => {
	let height = 5;
	let width = 10;
	let game = new Game(height, width);
	let rowCount = game.cells.length;
	let colCount = game.cells[0].length;
		
	expect(rowCount).toEqual(height);
	expect(colCount).toEqual(width);
});

test('Game should start with dead cells only', () => {
	let game = new Game(3, 3);
	
	game.cells.forEach(row => {
		row.forEach(cell => {
			expect(cell.alive).toBe(false);
		});
	});
});

test('Toggling a dead cell should set it to alive', () => {
	let game = new Game(3, 3);
	let cell = game.cells[0][0]; 
	
	cell.toggle();
	
	expect(cell.alive).toBe(true);
});

test('Toggling an alive cell should set it to dead', () => {
	let game = new Game(3, 3);
	let cell = game.cells[0][0]; 
	
	cell.toggle();
	cell.toggle();
	
	expect(cell.dead).toBe(true);
});

test('Setting an alive cell state with an aliveCount of 1 should set it to dead', () => {
	let game = new Game(3, 3);
	let aliveCount = 1;
	let cell = game.cells[0][0]; 
	
	cell.toggle();
	cell.setState(aliveCount);
	
	expect(cell.dead).toBe(true);
});

test('Setting an alive cell state with an aliveCount of 2 should set it to alive', () => {
	let game = new Game(3, 3);
	let aliveCount = 2;
	let cell = game.cells[0][0]; 
	
	cell.toggle();
	cell.setState(aliveCount);
	
	expect(cell.alive).toBe(true);
});

test('Setting an alive cell state with an aliveCount of 3 should leave it to alive', () => {
	let game = new Game(3, 3);
	let aliveCount = 3;
	let cell = game.cells[0][0]; 
	
	cell.toggle();
	cell.setState(aliveCount);
	
	expect(cell.alive).toBe(true);
});

test('Setting an alive cell state with an aliveCount of more than 3 should set it to dead', () => {
	let game = new Game(3, 3);
	let aliveCount = 6;
	let cell = game.cells[0][0]; 
	
	cell.toggle();
	cell.setState(aliveCount);
	
	expect(cell.dead).toBe(true);
});

test('Setting a dead cell state with an aliveCount of 3 should set it to alive', () => {
	let game = new Game(3, 3);
	let aliveCount = 3;
	let cell = game.cells[0][0]; 
	
	cell.setState(aliveCount);
	
	expect(cell.alive).toBe(true);
});

test('Setting a dead cell state with an aliveCount of other than 3 should set it to dead', () => {
	let game = new Game(3, 3);
	let aliveCount = 7;
	let cell = game.cells[0][0]; 
	
	cell.setState(aliveCount);
	
	expect(cell.dead).toBe(true);
});

test('A cell should have 8 neighbours', () => {
	let game = new Game(3, 3);
	let cell = game.cells[1][1];
	let neighbors = cell.neighbors(game);
	
	expect(neighbors).toHaveLength(8);
});

test('A cell neighbours should be top, topright, right, bottomright, bottom, bottomleft, left, topleft', () => {
	let game = new Game(3, 3);
	let cell = game.cells[1][1];
	let neighbors = cell.neighbors(game);
	
	expect(neighbors[0].toString()).toEqual('row: 0, col: 1, state: 0');
	expect(neighbors[1].toString()).toEqual('row: 0, col: 2, state: 0');
	expect(neighbors[2].toString()).toEqual('row: 1, col: 2, state: 0');
	expect(neighbors[3].toString()).toEqual('row: 2, col: 2, state: 0');
	expect(neighbors[4].toString()).toEqual('row: 2, col: 1, state: 0');
	expect(neighbors[5].toString()).toEqual('row: 2, col: 0, state: 0');
	expect(neighbors[6].toString()).toEqual('row: 1, col: 0, state: 0');
	expect(neighbors[7].toString()).toEqual('row: 0, col: 0, state: 0');
});

test('A game board should stitch back onto itself', () => {
	let game = new Game(1, 1);
	let cell = game.cells[0][0];
	let neighbors = cell.neighbors(game);
	
	expect(neighbors[0].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[1].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[2].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[3].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[4].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[5].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[6].toString()).toEqual('row: 0, col: 0, state: 0');
	expect(neighbors[7].toString()).toEqual('row: 0, col: 0, state: 0');
});

test('Seeding a board should assign alive and dead status randomly', () => {
	let game1 = new Game(3, 3);
	let game2 = new Game(3, 3);
	
	game1.seed();
	game2.seed();
	
	expect(game1.toString()).not.toEqual(game2.toString());
});

test('Clearing a board should set all cells to dead', () => {
	let game = new Game(3, 3);
	
	game.seed();
	game.clear();
	game.cells.forEach(row => {
		row.forEach(cell => {
			expect(cell.dead).toBe(true);	
		});
	});
});
