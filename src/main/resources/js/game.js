'use strict';

const ALIVE = 1;
const DEAD = 0;
    
class Game {
    constructor(height, width) {
        this.height = height;
        this.width = width;
        this.cells = [];        
        this.init();
    }

    init() {
        this.cells = [];
        for (let i = 0; i < this.height; i++) {
            this.cells.push([]);
            for (let j = 0; j < this.width; j++) {
                this.cells[i].push(new Cell(i, j));
            }
        }
    }
    
    clear() {
        this.init();
    }
    
    cloneCells() {
    	return this.cells.map(row => {
    		return row.map(cell => {
    			return new Cell(cell.row, cell.col, cell.state);
    		});
    	});
    }
      
    step() {
    	let clones = this.cloneCells();
    	
        this.cells.forEach((row, i) => {
            row.forEach((cell, j) => {
                let neighbors = cell.neighbors(this);
                let aliveCount = neighbors.filter(neighbor => neighbor.alive).length;
                
                clones[i][j].setState(aliveCount);
            });
        });
        this.cells = clones;
    }
    
    seed() {
        this.clear();
        this.cells.forEach(row => {
            row.forEach(cell => {
                let rnd = Math.floor(Math.random() * 2);
                
                if (rnd) {
                    cell.toggle();
                }
            });
        });
    }
    
    toString() {
    	let string = '';
    	this.cells.forEach(row => {
            row.forEach(cell => {
                string += `[${cell.alive ? 'X' : '0'}]`;
            });
            string += '\n';
        });
    	return string;
    }
}

class Cell {
 
    constructor(row, col, state) {
        this.row = row;
        this.col = col;
        this.state = state || 0;
    }
    
    get alive() {
        return this.state === ALIVE;
    }
    
    get dead() {
        return !this.alive;
    }
    
    setState(aliveCount) {
        if (this.alive) {
            if (aliveCount < 2 || aliveCount > 3) {
                this.state = DEAD;
            } else if (aliveCount === 2 || aliveCount === 3) {
                this.state = ALIVE;
            }
        } else if (aliveCount === 3) {
            this.state = ALIVE;
        }
    }
    
    neighbors(game) {
        let above = this.row === 0 ? game.height - 1 : this.row - 1;
        let below = this.row === game.height - 1 ? 0 : this.row + 1;
        let left = this.col === 0 ? game.width - 1 : this.col - 1;
        let right = this.col === game.width - 1 ? 0 : this.col + 1;
        
        return [
            game.cells[above][this.col],
            game.cells[above][right],
            game.cells[this.row][right],
            game.cells[below][right],
            game.cells[below][this.col],
            game.cells[below][left],
            game.cells[this.row][left],
            game.cells[above][left]
        ];
    }
    
    toggle() {
        this.state = this.alive ? DEAD : ALIVE;
    }
    
    toString() {
    	return `row: ${this.row}, col: ${this.col}, state: ${this.state}`;
    }
}

module.exports = {
		Game,
		Cell
};
