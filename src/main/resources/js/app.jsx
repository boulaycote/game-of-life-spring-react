'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {Game} from './game';
import Board from './board.jsx';

class App extends React.Component {
	
	constructor(props) {
	    super(props);
		this.game = new Game(25, 25);
		this.timeout = null;
	    
		this.seedBoardClick = this.seedBoard.bind(this);
		this.stepClick = this.step.bind(this);
		this.runClick = this.run.bind(this);
		this.stopClick = this.stop.bind(this);
		this.clearClick = this.clear.bind(this);
		this.cellClick = this.toggleCell.bind(this);
	  
        this.state = {
            board: this.game.cells 
        };
	}

	refresh() {
	    this.setState({
            board: this.game.cells
        });
	}
	
	clear() {
	    this.game.clear();
	    this.refresh();
	}
	
	seedBoard() {
	    this.game.seed();
	    this.refresh();
	}
	
	step() {
	    this.game.step();
	    this.refresh();
	}
	
	run() {
	    this.stop();
	    this.step();
	    this.timeout = setTimeout(this.run.bind(this), 100);
	}
	
	stop() {
	    clearTimeout(this.timeout);
	}
	
	toggleCell(cell) {
	    cell.toggle();
	    this.refresh();
	}
	
	render() {
	    return (
	        <div>
	            <p>Seed the board by clicking on cells or generate it <button onClick={this.seedBoardClick}>randomly</button>.</p>
	            <Board board={this.state.board} toggleCell={this.cellClick} />
	            <button onClick={this.stepClick}>step</button>
	            <button onClick={this.runClick}>run</button>
	            <button onClick={this.stopClick}>stop</button>
	            <button onClick={this.clearClick}>clear</button>
	        </div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById('react'));
