# Getting started
* Install `node` and `npm`.

#Running the app
* Then run `./gradlew build && java -jar build/libs/game-of-life-0.1.0.jar`.

#Running the app for development
* In one terminal `./gradlew build -t` this will rebuild the app as soon a file changes.
* In a second one run spring boot `./gradlew bootRun`.
